#!/bin/bash

# scan-to-ebook, преобразование сканированных документов в электронные книги
# Copyright (C) 2013 Andrey Zvorygin
# e-mail: umount.dev.brain@ya.ru
# Это программа является свободным программным обеспечением. Вы можете 
# распространять и/или модифицировать её согласно условиям Стандартной 
# Общественной Лицензии GNU, опубликованной Фондом Свободного Программного 
# Обеспечения, версии 3 или, по Вашему желанию, любой более поздней версии. 
# Эта программа распространяется в надежде, что она будет полезной, но БЕЗ 
# ВСЯКИХ ГАРАНТИЙ, в том числе подразумеваемых гарантий ТОВАРНОГО СОСТОЯНИЯ ПРИ 
# ПРОДАЖЕ и ГОДНОСТИ ДЛЯ ОПРЕДЕЛЁННОГО ПРИМЕНЕНИЯ. Смотрите Стандартную 
# Общественную Лицензию GNU для получения дополнительной информации. 
# Вы должны были получить копию Стандартной Общественной Лицензии GNU вместе 
# с программой. В случае её отсутствия, посмотрите <http://www.gnu.org/licenses/>.

# Подгрузим конфигурацию
project_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$project_dir/_params"
# По умолчанию конвертируем изображения в djvu
output_format="djvu"
# Нужно ли пропускать создание уже сгенерированных книг
force_overwrite=0

show_help() {
  echo "
Andrey Zvorygin, 2012-2013
email: umount.dev.brain@ya.ru

scan-to-ebook - скрипт для преобразования каталогов с изображениями в
форматы PDF или DJVU.
Для работы скрипту необходимы ImageMagick, djvulibre и pdftk.

Использование: scan-to-ebook [OPTIONS]

Список параметров:
-s, --source-path <path>              Указывает директорию, из которой будут
                                      браться изображения. По умолчанию исполь-
                                      зуется текущая директория.

-d, --destination-path <path>         Указывает, в какую директорию будут скла-
                                      дываться готовые файлы. По умолчанию ис-
                                      пользуется текущая директория.

-a, --additional-check-paths <paths>  Указывает директории, в которых могут на-
                                      ходиться уже собранные файлы. Список раз-
                                      деляется точкой с запятой. В таком случае
                                      обрабатываемая директория пропускается.

-o, --output-format <pdf|djvu>        Указывает, в какой формат конверитировать
                                      изображения. Формат по умолчанию - djvu.

-f, --force-overwrite                 Пересоздавать уже сгенерированные элек-
                                      тронные книги

-q, --quiet                           Не выводит в терминал сообщения о процес-
                                      се выполнения задачи.

-h, --help                            Помощь по скрипту

-v, --version                         Вывод версии
"
  exit 0
}

opts=`getopt -o s:d:o:a:hqfv -l source-path:,destination-path:,output-format:,additional-check-paths:,help,quiet,force-overwrite,debug,version -- "$@"`
if [ $? != 0 ]; then
  exit 1
fi

eval set -- "$opts"

while [ ! -z "$1" ] ; do
  case "$1" in
    -s|--source-path) src_path="$2"; shift 2;;
    -d|--destination-path) dst_path="$2"; shift 2;;
    -a|--additional-check-paths) additional_check_paths="$2"; shift 2;;
    -o|--output-format) output_format="$2"; shift 2;;
    -q|--quiet) quiet=1; shift;;
    -f|--force-overwrite) force_overwrite=1; shift;;
    -h|--help) show_help; shift;;
    -v|--version) get_version; shift;;
    --debug) debug=1; shift;;
    *) break;;
  esac
done

check_additional_paths "$additional_check_paths"
# Перевести значение переменной $output_format в нижний регистр
output_format=$(echo "$output_format" | tr '[A-Z]' '[a-z]')
case $output_format in
  djvu ) debug_message "Будут генерироваться файлы djvu";;
  pdf ) debug_message "Будут генерироваться файлы pdf";;
  * ) error_message "Выбран неподдерживаемый выходной формат ($output_format)"
      exit 1;;
esac
check_src_dst_dir "$src_path" "$dst_path"
add_slash "$src_path" src_path; add_slash "$dst_path" dst_path
find "$src_path" -type d | sort -n | while read current_dir; do
  if [ "$current_dir" == "$src_path" ]; then
    debug_message "Непосредственно директория $current_dir не обрабатывается"
  else
    regular_message ""
    regular_message "Обработка директории $current_dir"
    target_file_name=$(echo "$current_dir.$output_format" | sed "s/$(escape_string "$src_path")//gI")
    debug_message "Относительный путь генерируемого файла: $target_file_name"
    target_file="$dst_path$target_file_name"
    debug_message "Абсолютный путь генерируемого файла: $target_file"
    if [ $force_overwrite -eq 0 ]; then
      # Если отключена принудительная перезапись, то нужно проверить,
      # есть ли генерируемый файл где-нибудь в дополнительных директориях
      check_in_paths "$additional_check_paths" "$target_file_name"
      if [ "$?" -eq 1 ]; then
        debug_message "Переход к следующей директории"
        continue
      fi
      # Если переменной created_pages в результате вычислений будет присвоено
      # пустое значение или ноль, значит, либо генерируемого файла не существует, либо он пустой
      case $output_format in
        djvu ) created_pages=$(djvu_file_pages_count "$target_file");;
        pdf ) created_pages=$(pdf_file_pages_count "$target_file");;
      esac
      if [ $created_pages -eq 0 ]; then
        debug_message "Файл $target_file поврежден, либо не существует. Генерация будет производиться с первой страницы"
        # На всякий случай, если все-таки поврежден
        rm -f "$target_file"
        created_pages=0
      else
        debug_message "Количество страниц в генерируемом файле: $created_pages"
      fi
    else
      debug_message "Включен режим принудительной перезаписи. Файл $target_file будет удален"
      created_pages=0
      rm -f "$target_file"
    fi
    current_dir_files_count=$(ls -a "$current_dir" | grep -i .jpg$ | wc -l)
    debug_message "Количество файлов в просматриваемой директории: $current_dir_files_count"
    # Генерация запускается, если в результирующем файле страниц меньше, чем
    # изображений в исходной папке
    if [ $created_pages -lt $current_dir_files_count ]; then
      prepare_directory "$(dirname "$target_file")"
      processed_pages=0
      current_dir_listing=$(find "$current_dir" -maxdepth 1 -iname "*.jpg" | sort -n)
      for current_page in $current_dir_listing; do
        processed_pages=$(($processed_pages + 1))
        # Обработать страницу, если ее нет в генерируемом файле
        if [ $processed_pages -gt $created_pages ]; then
          regular_message "Обработка и присоединение страницы номер $processed_pages"
          case $output_format in
            djvu )
              if [ $processed_pages -eq 1 ]; then
                c44 -dpi 300 -slice 74+13+10 "$current_page" "$target_file"
              else
                tmp_djvu_file=$(mktemp)
                c44 -dpi 300 -slice 74+13+10 "$current_page" "$tmp_djvu_file"
                djvm -i "$target_file" "$tmp_djvu_file"
                rm -f "$tmp_djvu_file"
              fi;;
            pdf ) 
              if [ $processed_pages -eq 1 ]; then
                convert "$current_page" "$target_file"
              else
                convert "$current_page" -quality 100 pdf:- | pdftk "$target_file" - cat output "$target_file.for-rename"
                mv "$target_file.for-rename" "$target_file"
              fi;;
          esac
        fi
      done
    else
      regular_message "Все изображения уже были обработаны"
    fi
  fi
done
exit 0